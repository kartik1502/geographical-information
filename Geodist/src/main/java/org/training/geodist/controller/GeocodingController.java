package org.training.geodist.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.geodist.dto.DistanceRequest;
import org.training.geodist.dto.DistanceResponse;
import org.training.geodist.service.GeoDistance;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/geodist")
@RequiredArgsConstructor
public class GeocodingController {

	private final GeoDistance geoDistance;

	@PostMapping("/distance")
	public ResponseEntity<DistanceResponse> getDstance(@RequestBody DistanceRequest distanceRequest) {

		return ResponseEntity.ok(geoDistance.getDistance(distanceRequest));
	}
}

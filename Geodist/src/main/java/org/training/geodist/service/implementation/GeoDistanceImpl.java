package org.training.geodist.service.implementation;

import org.springframework.stereotype.Service;
import org.training.geodist.dto.DistanceRequest;
import org.training.geodist.dto.DistanceResponse;
import org.training.geodist.service.GeoDistance;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class GeoDistanceImpl implements GeoDistance {

	@Override
	public DistanceResponse getDistance(DistanceRequest distanceRequest) {
	
		double dLat = Math.toRadians(distanceRequest.destinationLatitude() - distanceRequest.sourceLatitude());
		double dLong = Math.toRadians(distanceRequest.destinationLongitude() - distanceRequest.sourceLongitude());
		
		double sourceLatitude = Math.toRadians(distanceRequest.sourceLatitude());
		double destinationLatitude = Math.toRadians(distanceRequest.destinationLatitude());
		
		double innerSqrt = Math.pow(Math.sin(dLat / 2), 2) + 
                Math.pow(Math.sin(dLong / 2), 2) * 
                Math.cos(sourceLatitude) * 
                Math.cos(destinationLatitude);
		
     double earthRadius = 6371;
     double c = 2 * Math.asin(Math.sqrt(innerSqrt));
     return new DistanceResponse("200", earthRadius * c);
	}

	
	
}

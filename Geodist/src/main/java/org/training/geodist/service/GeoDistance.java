package org.training.geodist.service;

import org.training.geodist.dto.DistanceRequest;
import org.training.geodist.dto.DistanceResponse;

public interface GeoDistance {

	DistanceResponse getDistance(DistanceRequest distanceRequest);

}

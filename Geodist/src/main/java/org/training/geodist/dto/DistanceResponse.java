package org.training.geodist.dto;

public record DistanceResponse(String code, Double distance) {

}

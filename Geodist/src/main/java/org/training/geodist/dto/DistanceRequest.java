package org.training.geodist.dto;

public record DistanceRequest(double sourceLatitude, double sourceLongitude, double destinationLatitude,
		double destinationLongitude) {

}
package org.training.geodist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class GeodistApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeodistApplication.class, args);
	}

}
